# PARSER PACKAGES

# Following packages support only 32-bit compilation.
# When 32-bit multilib is enabled, compile them with lib32- prefix.

# VIDEO Packages
IMAGE_INSTALL += "${@bb.utils.contains("MACHINE_FEATURES", "qti-display", "lib32-media", "",d)}"
IMAGE_INSTALL += "${@bb.utils.contains("MACHINE_FEATURES", "qti-display", "media", "",d)}"
IMAGE_INSTALL_remove += "${@bb.utils.contains('MULTILIB_VARIANTS', 'lib32', 'media', 'lib32-media', d)}"
