inherit autotools pkgconfig

DESCRIPTION = "media"

LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/${LICENSE};md5=3775480a712fc46a69647678acb234cb"

FILESPATH =+ "${WORKSPACE}:"
SRC_URI = "file://hardware/qcom/media/"
SRC_URI  += "file://msm8953-version.sh"

S = "${WORKDIR}/hardware/qcom/media"

DEPENDS += "display-hal-linux"
DEPENDS += "libcutils"
DEPENDS += "binder"
DEPENDS += "virtual/kernel"
DEPENDS += "libion"
DEPENDS += "media-headers libbase"

PACKAGECONFIG ?= "glib ${@bb.utils.contains('MACHINE_FEATURES', 'perceptual-quantization', 'pq', '', d)} ${@bb.utils.contains('MACHINE_FEATURES', 'vqzip', 'vqzip', '', d)} ${@bb.utils.contains('DISTRO_FEATURES', 'qti-video-encoder', 'mm-venc', '', d)} ${@bb.utils.contains('DISTRO_FEATURES', 'qti-video-decoder', 'mm-vdec', '', d)} ${@bb.utils.contains('MACHINE_FEATURES', 'ubwc', 'ubwc', '', d)} gbm ion ${@bb.utils.contains('MACHINE_FEATURES', 'hypervisor', 'hypervisor', '', d)}"

PACKAGECONFIG[mm-venc] = "--enable-venc, --disable-venc"
PACKAGECONFIG[mm-vdec] = "--enable-vdec, --disable-vdec"
PACKAGECONFIG[glib]    = "--enable-use-glib, --disable-use-glib, glib-2.0, glib-2.0"
PACKAGECONFIG[media-extensions] = "--enable-target-uses-media-extensions, --disable-target-uses-media-extensions"
PACKAGECONFIG[pq]      = "--enable-targets-that-support-pq,  --disable-targets-that-support-pq"
PACKAGECONFIG[vqzip]   = "--enable-targets-that-support-vqzip, --disable-targets-that-support-vqzip"
PACKAGECONFIG[ubwc]    = "--enable-is-ubwc-supported, --disable-is-ubwc-supported"
#PACKAGECONFIG[adsp-pq] = "--enable-targets-that-support-adsp-pq, --disable-targets-that-support-adsp-pq"
PACKAGECONFIG[gbm]     = "--enable-targets-use-libgbm, --disable-targets-use-libgbm, gbm, gbm"
PACKAGECONFIG[ion]     = "--enable-target-uses-ion, --disable-target-uses-ion"
PACKAGECONFIG[hypervisor] = "--enable-target-hypervisor, --disable-target-hypervisor"
PACKAGECONFIG[master-side-cp] = "--enable-master-side-cp-target-list, --disable-master-side-cp-target-list"

RDEPENDS_${PN} = "libbase libcutils binder "

MAJOR_KV = "${@d.getVar('PREFERRED_VERSION_linux-msm', True)}"

do_configure[depends] += "virtual/kernel:do_shared_workdir"

# configure features
EXTRA_OECONF +=' --enable-target-${SOC_FAMILY}="yes" \
                 --enable-target-kernel-version=${MAJOR_KV} \
                 --enable-targets-that-use-flag-msm8226="yes" \
                 --with-ui-headers=${STAGING_INCDIR}/ui/ \
                 --with-android-headers=${STAGING_INCDIR}/ \
                 --with-utils-headers=${STAGING_INCDIR}/utils/ \ 
                 --with-cutils-headers=${STAGING_INCDIR}/cutils/ \
                 --with-glib-headers=${STAGING_INCDIR}/glib-2.0/ \
                 --with-binder-headers=${STAGING_INCDIR}/binder/ \
                 --with-adreno-headers=${STAGING_INCDIR}/adreno/ \
                 --with-glib-lib-dir=${STAGING_LIBDIR}/glib-2.0/include \
                 --with-gralloc-headers=${STAGING_INCDIR}/libgralloc/ \
                 --with-qdutils-headers=${STAGING_INCDIR}/libqdutils/ \
                 --with-libgpustats-headers=${STAGING_INCDIR}/libgpustats/ \
                 --with-libpqstats-headers=${STAGING_INCDIR}/libpqstats/ \
                 --with-libvqzipstats-headers=${STAGING_INCDIR}/libvqzip/ \
                 --with-sanitized-headers=${STAGING_KERNEL_BUILDDIR}/usr/include \
                 --with-display-headers=${STAGING_INCDIR}/qcom/display \
                 --with-android-headers=${STAGING_INCDIR}/ \
'

FILES_${PN}-dbg  = "${libdir}/.debug/*"
FILES_${PN}      = "${libdir}/*.so ${libdir}/*.so.* ${libdir}/*.so.*.*.* ${sysconfdir}/* ${bindir}/* ${libdir}/pkgconfig/*"
FILES_${PN}-dev  = "${libdir}/*.la ${includedir}"
do_install_append() {
    oe_runmake DESTDIR="${D}/" LIBVER="${LV}" install
    mkdir -p ${STAGING_INCDIR}/mm-core
    mkdir -p ${STAGING_INCDIR}/libstagefrighthw
    install -m 0644 ${S}/mm-core/inc/*.h ${STAGING_INCDIR}/mm-core
    install -m 0644 ${S}/libstagefrighthw/*.h ${STAGING_INCDIR}/libstagefrighthw
}

do_install_append_apq8053() {
    install -m 0755 ${WORKDIR}/msm8953-version.sh -D ${D}${sysconfdir}/init.d/msm8953-version.sh
}

pkg_postinst_${PN}_apq8053 () {
    [ -n "$D" ] && OPT="-r $D" || OPT="-s"
    # remove all rc.d-links potentially created from alternatives
    update-rc.d $OPT -f msm8953-version.sh remove
    update-rc.d $OPT msm8953-version.sh start 60 2 3 4 5 .
}

INSANE_SKIP_${PN} += "dev-so"
EXCLUDE_FROM_SHLIBS = "1"
