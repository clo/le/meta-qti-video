SUMMARY = "QTI Video opensource package groups"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

LICENSE = "BSD-3-Clause"

PROVIDES = "${PACKAGES}"

PACKAGES = ' \
    packagegroup-qti-video \
    packagegroup-qti-media \
'

RDEPENDS_packagegroup-qti-video += " \
    ${@bb.utils.contains('COMBINED_FEATURES', 'qti-video', 'packagegroup-qti-media', '', d)} \
"

RDEPENDS_packagegroup-qti-media = " \
    media \
"
